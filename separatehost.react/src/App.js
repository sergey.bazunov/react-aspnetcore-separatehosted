import './App.css';
import React, {useState, useEffect} from 'react';

function App() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch("https://localhost:5001/WeatherForecast")
    .then(response => response.json())
    .then(data => {
      console.log(data)      
      setItems(data)
    })
    .catch(e => {
      console.log(e);
    })
  }, [])

  return (   
    <div className="App">
      <header className="App-header">      
      </header>
      <main>
      <table border={1}>
        <thead>
          <tr>
            <th>Date</th>
            <th>Temp. (C)</th>
            <th>Temp. (F)</th>
            <th>Summary</th>
          </tr>
        </thead>
        <tbody>
          {items.map(forecast =>
            <tr key={forecast.date}>
              <td>{forecast.date}</td>
              <td>{forecast.temperatureC}</td>
              <td>{forecast.temperatureF}</td>
              <td>{forecast.summary}</td>
            </tr>
          )}
        </tbody>
      </table>        
      </main>
    </div>          
  );
}

export default App;
